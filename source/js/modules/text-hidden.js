'use strict';

(function() {
        // const textButton = document.querySelectorAll('.js-text-button');

        // textButton.forEach((button) => {
        //     button.addEventListener('click' , (event) => {
        //         event.preventDefault();
        //         let text = button.parentElement.parentElement.querySelector('.js-text-block');
        //         let bigHeight = text.scrollHeight;
        //         if (text.classList.contains('active')) {
        //             text.classList.remove('active');
        //             text.removeAttribute("style");
        //         } else {
        //             text.classList.add('active');
        //             text.style.maxHeight = `${bigHeight}px`;
        //         }
        //     })
        // })

    $(document).ready(function () {
        $('.js-text-button').click(function (e) {
            e.preventDefault();
            $(this).toggleClass('active')
            $(this).parent().find('.js-text-hidden').slideToggle();
            if($(this).hasClass('active')) {
                $(this).text("Свернуть");
            } else {
                $(this).text("Развернуть");
            }
        });
    });
}());
