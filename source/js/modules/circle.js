$(document).ready(function () {
    $('.js-circle-people').each(function(i , item) {
        $(item).circleProgress({
            value: $(item).data('value'),
            lineCap: "round",
            size: 64,
            startAngle: Math.PI / 2,
            fill: {
                 color: $(item).data('color')
            },
            emptyFill: "#EEEEEE"
        });
    });
    $('.js-circle-expert').each(function(i , item) {
        $(item).circleProgress({
            value: $(item).data('value'),
            lineCap: "round",
            size: 64,
            reverse: true,
            startAngle: Math.PI / 2,
            fill: {
                 color: "#D63649"
            },
            emptyFill: "#EEEEEE"
        });
    });
    $('.js-circle').each(function(i , item) {
        $(item).circleProgress({
            value: $(item).data('value'),
            // lineCap: "round",
            startAngle: - Math.PI / 2,
            size: 84,
            fill: {
                 color: $(item).data('color')
            },
            emptyFill: "#EEEEEE"
        });
    });

    $('.js-circle-preview').each(function(i , item) {
        $(item).circleProgress({
            value: $(item).data('value'),
            // lineCap: "round",
            startAngle: - Math.PI / 2,
            size: $(item).data('size'),
            fill: {
                 color: $(item).data('color')
            },
            emptyFill: "transparent",
            thickness: 1
        });
    });

    $('.js-circle-for-card').each(function(i , item) {
        $(item).circleProgress({
            value: $(item).data('value'),
            // lineCap: "round",
            startAngle: - Math.PI / 2,
            size: $(item).data('size'),
            fill: {
                 color: $(item).data('color')
            },
            emptyFill: "#D9EAFA",
            thickness: 1
        });
    });

    $('.js-big-circle').each(function(i , item) {
        $(item).circleProgress({
            value: $(item).data('value'),
            // lineCap: "round",
            startAngle: - Math.PI / 2,
            size: 146,
            fill: {
                 color: $(item).data('color')
            },
            emptyFill: "#EEEEEE"
        });
    });
});
