// 'use strict'

(function () {
    const btn = document.querySelector('.header__form-button');
    const input = document.querySelector('.header__form-button input');
    const list = document.querySelector('.header__search-list');

    input.addEventListener('input' , () => {
        if (input.value !== "") {
            btn.classList.add('input-active');
            list.classList.remove('hidden');
        } else {
            btn.classList.remove('input-active');
            list.classList.add('hidden');
        }
    })

})();
