'use strict';

(function() {
    $(window).on("scroll", function() {
        var currentPos = $(window).scrollTop();
        $('.overview-nav li a').each(function() {
            var sectionLink = $(this);
          // capture the height of the navbar
            var navHeight = $('#nav-wrapper').outerHeight() + 1;

            sectionLink.click(function(e) {
                $('html,body').stop().animate({ scrollTop: $(section).offset().top - navHeight + 0 }, 1000);
                e.preventDefault();
            });

          var section = $(sectionLink.attr('href'));

          // subtract the navbar height from the top of the section
          if(section.position().top - navHeight  <= currentPos && section.offset().top + section.height()> currentPos + 100) {
                $('.nav a').removeClass('active');
                sectionLink.addClass('active');
            } else {
                sectionLink.removeClass('active');
            }
        });
   });
}());
