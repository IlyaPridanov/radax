$( function() {
    $(document).ready(function () {
        if ($( ".js-slider-range" )) {
            $( ".js-slider-range" ).each(function (i,item) {
                let input1 = $(item).parent().find('.js-slider-input-1');
                let input2 = $(item).parent().find('.js-slider-input-2');
                let min = $(item).data("min");
                let max = $(item).data("max");
                let start = $(item).data("start");
                let end = $(item).data("end");
                $(item).slider({
                    range: true,
                    min: min,
                    max: max,
                    values: [ start, end ],
                    slide: function( event, ui ) {
                        $(input1).val(ui.values[ 0 ]);
                        $(input2).val(ui.values[ 1 ]);
                    }
                });
                $(input1).val($(item).slider( "values", 0 ));
                $(input2).val($(item).slider( "values", 1 ));

            });
        }

    });
} );
