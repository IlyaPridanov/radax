'use strict';

(function() {
    var sliderContainers = $('.slider');

    var getBlockSlider = function (slider) {
        if (slider) {
            var container = $(slider).find('.slider-row');
            var prev = $(slider).find('.slide-btn-next');
            var next = $(slider).find('.slide-btn-prev');

            var show = 3;

            if (slider.classList.contains('raiting')) {
                show = 1;
            }

            container.slick({
                infinite: true,
                slidesToShow: show,
                slidesToScroll: 1,
                prevArrow: prev,
                nextArrow: next,
            });
        }
      };

      sliderContainers.each(function (index) {
        getBlockSlider(this);
      });
}());
