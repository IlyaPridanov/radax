$(document).ready(function() {
    $('[data-gallery]').fancybox({
        openEffect  : 'none',
        closeEffect : 'none',
        arrows : 'true'
    });
    var $card = $('.js-fancy-min img');
    var $cardBig = $('.js-fancy-big');

    var $cardJson = [];

    $card.each(function(i,value){
        $cardJson[i] = {
            src: value.getAttribute('src'),
            opts: {
                thumb: value.getAttribute('src')
            }
        }
    });

    $cardBig.on('click', function() {
        $.fancybox.open($cardJson,
        {
            loop : true,
            imageScale : true,
            index: $(this).attr('data-index') + '',
            thumbs : {
                autoStart : true
            },
            buttons: [
                "slideShow",
                "fullScreen",
                "thumbs",
                "close"
            ]
        });
    });
});
