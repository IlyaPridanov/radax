'use strict';

(function() {
    $(document).ready(function () {
        $('.js-tab-toggle').click(function () {
            $(this).parent().toggleClass('js-tab-item-active');
            $(this).toggleClass('js-tab-toggle-active').next().slideToggle();
        });

        $('.js-tab-toggle-btn').click(function () {
            $(this).parent().parent().toggleClass('js-tab-item-active');
            $(this).parent().toggleClass('js-tab-toggle-active').next().slideToggle();
        });

        if (window.matchMedia('(max-width: 576px)').matches) {
            $('.js-tab-mobile-toggle').click(function () {
                $(this).parent().toggleClass('js-tab-mobile-item-active');
                $(this).toggleClass('js-tab-mobile-toggle-active').next().slideToggle();
            });
        }
    });
}());
