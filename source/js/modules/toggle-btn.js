'use strict';

(function() {
    const btn = document.querySelectorAll('.toggle-btn');

    btn.forEach((item) => {
        item.addEventListener('click' , () => {
            item.classList.toggle('toggle-btn-right')
        })
    })

    const headerFormButton = document.querySelectorAll('.header__form-button');

    headerFormButton.forEach((item) => {
        item.addEventListener('click' , (evt) => {
            if (evt.target !== item.querySelector('input')) {
                item.classList.toggle('active');
                item.querySelector('input').focus();
            }
        })
    })

    const toggleBtn = document.querySelectorAll('.js-toggle');
    const toggleGroupBtn = document.querySelectorAll('.js-group-toggle');

    toggleBtn.forEach((item) => {
        item.addEventListener('click' , () => {
            item.classList.toggle('active')
        })
    })

    toggleGroupBtn.forEach((item) => {
        item.addEventListener('click' , () => {
            toggleGroupBtn.forEach((currentValue) => {
                currentValue.classList.toggle('active');
            })
        })
    })


}());
