ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
      center: [59.836269, 30.316955],
      controls: ["zoomControl"],
      zoom:5
    }),

    myPlacemark1 = new ymaps.Placemark([59.836269, 30.316955], {
      hintContent: 'Филиал в Санкт-Петербурге',

    }, {
      iconLayout: 'default#image',
      iconImageHref: '../img/icon-map-red.svg',
      iconImageSize: [27, 33],
      iconImageOffset: [-13, -15],
      balloonMaxWidth: 300,
      balloonOffset: [120,100],
      hideIconOnBalloonOpen: false

    }),

    myPlacemark2 = new ymaps.Placemark([56.813118, 60.591025], {
      hintContent: 'Филиал в Екатеринбурге',

    }, {
      iconLayout: 'default#image',
      iconImageHref: '../img/icon-map-red.svg',
      iconImageSize: [27, 33],
      iconImageOffset: [-13, -15],
      balloonMaxWidth: 300,
      balloonOffset: [120,100],
      hideIconOnBalloonOpen: false
    }),

    myPlacemark3 = new ymaps.Placemark([53.189188, 50.250230], {
      hintContent: 'Филиал в Самаре',

    }, {
      iconLayout: 'default#image',
      iconImageHref: '../img/icon-map-red.svg',
      iconImageSize: [27, 33],
      iconImageOffset: [-13, -15],
      balloonMaxWidth: 300,
      balloonOffset: [120,100],
      hideIconOnBalloonOpen: false
    });

    myPlacemark4 = new ymaps.Placemark([53.892978, 27.587074], {
        hintContent: 'Филиал в Минске',

      }, {
        iconLayout: 'default#image',
        iconImageHref: '../img/icon-map-red.svg',
        iconImageSize: [27, 33],
        iconImageOffset: [-13, -15],
        balloonMaxWidth: 300,
        balloonOffset: [120,100],
        hideIconOnBalloonOpen: false
      });

      myPlacemark5 = new ymaps.Placemark([51.188390, 71.526854], {
        hintContent: 'Филиал в Астане',

      }, {
        iconLayout: 'default#image',
        iconImageHref: '../img/icon-map-red.svg',
        iconImageSize: [27, 33],
        iconImageOffset: [-13, -15],
        balloonMaxWidth: 300,
        balloonOffset: [120,100],
        hideIconOnBalloonOpen: false
      });

      myPlacemark6 = new ymaps.Placemark([55.842438, 37.373347], {
        hintContent: 'Центральный офис',

      }, {
        iconLayout: 'default#image',
        iconImageHref: '../img/icon-map-red.svg',
        iconImageSize: [27, 33],
        iconImageOffset: [-13, -15],
        balloonMaxWidth: 300,
        balloonOffset: [120,100],
        hideIconOnBalloonOpen: false
      });

    myMap.geoObjects
    .add(myPlacemark1)
    .add(myPlacemark2)
    .add(myPlacemark3)
    .add(myPlacemark4)
    .add(myPlacemark5)
    .add(myPlacemark6);

    myMap.behaviors.disable('scrollZoom');
  });
